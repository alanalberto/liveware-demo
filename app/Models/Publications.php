<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publications extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'user_id',
        'Title',
        'Description',
        'Path_picture'
    ];

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}
