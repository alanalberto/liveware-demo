<?php

namespace App\Http\Livewire\Post;

use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Livewire\Component;
use App\Models\Publications;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PublicationsComponent extends Component
{

    use WithPagination,WithFileUploads;

    public $post_id;
    public $title;
    public $img;
    public $path_img = null;
    public $description;
    public $createPost = false;
    public $editPost = false;

    protected $rules = [
        'title' => 'required',
        'description' => 'required',
        /* 'img' => 'image|max:1024', // 1MB Ma */
    ];

    public function render()
    {
        return view('livewire.post.publications-component',[
            'Posts' => Publications::where("user_id",Auth::id())->paginate(3)
        ]);
    }
    
    public function clear()
    {
        $this->title = null;
        $this->description = null;
        $this->post_id = null;
        $this->img = null;
    }

    public function savePublication(){

        $this->validate();


        $file = $this->img;

        if($file == null){
            $path = null;
        }else{
            $path = $this->img->store('/imagenes','public');
        }

        //$path = $this->img->store('/imagenes','public');

        Publications::create([
            'user_id'=>Auth::id(),
            'Title' => $this->title,
            'Description' => $this->description,
            'Path_picture' => $path            
        ]);
        $this->createPost = false;
        $this->clear();        
    }


    public function saveEditPublication($id){

        $this->validate();

        $file = $this->img;

        if($file == null){
            $path = $this->path_img;
        }else{
            $path = $this->img->store('/imagenes','public');
        }

        Publications::where('id',$id)->update([            
            'Title' => $this->title,
            'Description' => $this->description,
            'Path_picture' => $path            
        ]);
        $this->createPost = false;
        $this->editPost = false;
        $this->clear();        
    }

    

    public function create()
    {
        $this->createPost = true;
        $this->img = null;
    }

    public function edit($id)
    {
        $this->createPost = true;
        $this->editPost = true;
        $Post = Publications::find($id);

        $this->title = $Post->Title;
        $this->description = $Post->Description;
        $this->post_id = $Post->id;
        $this->path_img = $Post->Path_picture;        
    }
    public function cancel()
    {
        $this->createPost = false;
        $this->editPost = false;
        $this->clear();  
    }

    public function delete($id)
    {
        Publications::find($id)->delete();
    }
}
