<div class="p-5">


    <div class="mt-10 sm:mt-0">
        <div class="md:grid md:grid-cols-3 md:gap-6">
            <div class="md:col-span-1">
                <div class="px-4 sm:px-0">
                    <h3 class="text-lg font-medium leading-6 text-gray-900">
                        @if($editPost == false)
                        Crear publicación
                        @else
                        Editar publicación
                        @endif
                    </h3>
                    <p class="mt-1 text-sm leading-5 text-gray-600">
                        @if($editPost == false)
                        formulario para crear una publicación
                        @else
                        formulario para editar una publicación
                        @endif
                    </p>
                </div>
            </div>
            <div class="mt-5 md:mt-0 md:col-span-2">
                {{-- <form wire:submit.prevent="savePublication"> --}}
                <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-6 sm:col-span-3">
                                <label for="first_name"
                                    class="block text-sm font-medium leading-5 text-gray-700">Titulo</label>
                                <input wire:model="title"
                                    class="mt-1 form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                @error('title') <span class="error">{{ $message }}</span> @enderror
                            </div>

                            <div class="col-span-6 sm:col-span-3">
                                <label for="last_name"
                                    class="block text-sm font-medium leading-5 text-gray-700">Description</label>
                                <input wire:model="description"
                                    class="mt-1 form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                @error('description') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>


                    @if($editPost == true)
                        @if($path_img!=null)
                            <img width="200px" height="200px" src="/storage/{{$path_img}}" alt="">
                            @else

                            @if ($img)
                                Photo Preview:
                                <img src="{{ $img->temporaryUrl() }}">
                            @endif

                        @endif
                    @else
                        {{-- @if ($img)
                            Photo Preview:
                            <img src="{{ $img->temporaryUrl() }}">
                        @endif --}}
                    @endif


                    <div class="px-4 py-5 bg-white sm:p-6">
                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-6 sm:col-span-3">
                                <label for="first_name"
                                    class="block text-sm font-medium leading-5 text-gray-700">Imagen</label>
                                <input type="file" wire:model="img"
                                    class="mt-1 form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5">
                                @error('img') <span class="error">{{ $message }}</span> @enderror
                            </div>

                        </div>


                        <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button wire:click="cancel()"
                                class="py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-gray-600 shadow-sm hover:bg-gray-500 focus:outline-none focus:shadow-outline-blue active:bg-gray-600 transition duration-150 ease-in-out">
                                Cancelar
                            </button>
                            @if($editPost == false)
                            <button wire:click="savePublication()"
                                class="py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 shadow-sm hover:bg-indigo-500 focus:outline-none focus:shadow-outline-blue active:bg-indigo-600 transition duration-150 ease-in-out">
                                Publicar
                            </button>
                            @else
                            <button wire:click="saveEditPublication({{$post_id}})"
                                class="py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 shadow-sm hover:bg-indigo-500 focus:outline-none focus:shadow-outline-blue active:bg-indigo-600 transition duration-150 ease-in-out">
                                Guardar
                            </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
