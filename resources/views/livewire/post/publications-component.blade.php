
<div>

    @if($editPost == false)
    <div class="w-full pt-10 flex justify-end ">
        <a href="#" wire:click="create()"
            class="md:my-0 py-1 px-10 bg-blue-800 text-white rounded-full md:mr-16 shadow-xl hover:bg-blue-900">Publicar</a>
    </div>
    @endif


    @if($createPost == false)
        @include('livewire.post.list_publications')
    @else
        @include('livewire.post.create_publication')
    @endif

</div>
