<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Users\UsersComponent;
use App\Http\Livewire\ChatComponent;
use App\Events\WebsocketDemoEvent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    broadcast(new WebsocketDemoEvent('some data'));
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');



Route::get('/users', UsersComponent::class)->name('users');

Route::get('/chat', ChatComponent::class)->name('chat');
